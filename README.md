自分が苦労した点
deviseを利用してログイン周りを実装する際、自分が欲しい情報がなかなか見つからず苦労した。
実装中、何度もエラーにぶつかり、エラー文を読んでエラー箇所と原因を見つけるのに相当時間がかかった。また、エラー文を検索しても英語のサイトしか出てこないことが多く、google翻訳を使いながら読み解いた。
初めてのゼロからのアプリ開発だったので、最初は何もわからず行き当たりばったりでコードを書いてしまい、エラーが出てもどこでエラーが出てるのかわからない状態が続いた。

学んだ点
行き当たりばったりでコードを書くのではなく、まず考えてからコードを書いた方が結果的に早く書けること。
英語をサイトを読めるようになることができれば開発に相当有利に働くこと。
ネットの記事を引用するときは更新日時とバージョンを確認しないとバージョンの変更でうまく動かないことがある。

自慢したい点、相談したい点。
自慢できる点はほぼ無いですが、ユーザー編集の際のストロングパラメータの追加を実装する際、的確に情報を探せてすんなり実装できた点が良かったです。

私は実務未経験からエンジニア転職を目指して勉強しているのですが、開発力をあげるにはオリジナルアプリをどんどん作っていった方が良いのでしょうか？
本なども少しづつ読んでいるのですが、本を使って開発しているとどうしても暗記になってしまうことが多く、身についている気があまりしないです。
また、エンジニアとして英語の文を読み解く力はやはり必須なのでしょうか。
