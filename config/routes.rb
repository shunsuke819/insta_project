Rails.application.routes.draw do
  get 'pages/:id', to:"pages#show", as: :mypage
  root 'pages#home'
  devise_for :users, controllers: {
        sessions: 'users/sessions', registrations: "users/registrations", omniauth_callbacks: 'users/omniauth_callbacks'
      }

  resources :users do
     member do
       get :following, :followers
     end
   end

  resources :photos, only: [:new, :create, :destroy]
  resources :relationships, only: [:create, :destroy]
end
