class PhotosController < ApplicationController

  def new
    @photo = current_user.photos.build
  end

  def show
    # @photo = Photo.find_by(user_id: current_user.id)
  end


  def create
    @photo = current_user.photos.build(photo_params)
    if @photo.save
      redirect_to root_url
    else
      redirect_to new_photo_path
    end
  end

  def destroy
    #未実装
  end

private
  def photo_params
    params.require(:photo).permit(:image)
  end

end
