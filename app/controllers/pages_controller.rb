class PagesController < ApplicationController
  def home
  end

  def show
    @user = User.find(params[:id])
    @photos = @user.photos
  end

  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render root_url
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render root_url
  end

end
