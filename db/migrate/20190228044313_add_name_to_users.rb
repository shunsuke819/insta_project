class AddNameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string
    add_column :users, :username, :string
    add_column :users, :website, :string
    add_column :users, :introduce, :text
    add_column :users, :phone, :string
    add_column :users, :gender, :string
  end
end
